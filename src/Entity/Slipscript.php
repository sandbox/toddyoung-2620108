<?php
/**
 * @file
 * Contains \Drupal\slipscript\Entity\Slipscript.
 */

namespace Drupal\slipscript\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\slipscript\SlipscriptInterface;

/**
 * Defines the Slipscript entity.
 *
 * @ConfigEntityType(
 *   id = "slipscript",
 *   label = @Translation("Slipscript"),
 *   handlers = {
 *     "list_builder" = "Drupal\slipscript\Controller\SlipscriptListBuilder",
 *     "form" = {
 *       "add" = "Drupal\slipscript\Form\SlipscriptForm",
 *       "edit" = "Drupal\slipscript\Form\SlipscriptForm",
 *       "delete" = "Drupal\slipscript\Form\SlipscriptDeleteForm"
 *     }
 *   },
 *   config_prefix = "slipscript",
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "route" = "route",
 *     "note" = "note",
 *   },
 *   links = {
 *     "edit-form" = "/admin/config/system/slipscript/{slipscript}",
 *     "delete-form" = "/admin/config/system/slipscript/{slipscript}/delete"
 *   }
 * )
 */
class Slipscript extends ConfigEntityBase implements SlipscriptInterface {

  /**
   * The Slipscript ID.
   *
   * @var string
   */
  public $id;

  /**
   * The Slipscript route.
   *
   * @var string
   */
  public $route;

  /**
   * The Slipscript note.
   *
   * @var string
   */
  public $note;

  // Your specific configuration property get/set methods go here,
  // implementing the interface.
}
?>