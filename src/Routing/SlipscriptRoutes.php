<?php
/**
 * @file
 * Contains \Drupal\slipscript\Routing\SlipscriptRoutes.
 */

namespace Drupal\slipscript\Routing;
use Symfony\Component\Routing\Route;
/**
 * Defines dynamic routes.
 */
class SlipscriptRoutes {

  /**
   * {@inheritdoc}
   */
  public function routes() {
		$storage = \Drupal::entityManager()->getStorage('slipscript');
		$slipscripts = $storage->loadMultiple();
	
		$routes = array();
		foreach($slipscripts as $slipscript) {
			$verb = $slipscript->route;
			$str = 'slipscript.' . $verb;
			$routes[$str] = new Route(
			  // path
			  '/' . $verb . '/{arg1}/{arg2}/{arg3}/{arg4}/{arg5}/{arg6}/{arg7}/{arg8}/{arg9}',
	  	  
				// defaults
				array(
				  '_controller' => '\Drupal\slipscript\Controller\SlipscriptController::includer',
					'_title' => '',
					'verb' => $verb,
					'arg1' => '', 'arg2' => '', 'arg3' => '',
					'arg4' => '', 'arg5' => '', 'arg6' => '',
					'arg7' => '', 'arg8' => '', 'arg9' => '',
				),
				
			  // requirements
				array('_permission' => 'access content')
			);
		}
		
		return $routes;
	}
}
?>