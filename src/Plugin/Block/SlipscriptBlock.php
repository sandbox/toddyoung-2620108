<?php

namespace Drupal\slipscript\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'Slipscript' block.
 *
 * @Block(
 *   id = "slipscript_block",
 *   admin_label = @Translation("Slipscript block"),
 * )
 */

class SlipscriptBlock extends BlockBase {
  
  /**
   * {@inheritdoc}
   */
  public function build() {    
    return array(
      '#markup' => $this->t('Hello World!'),
    );
  }
	
  
}

