<?php
/**
 * @file
 * Contains \Drupal\slipscript\SlipscriptInterface.
 */

namespace Drupal\slipscript;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining a Slipscript entity.
 */
interface SlipscriptInterface extends ConfigEntityInterface {
  // Add get/set methods for your configuration properties here.
}
?>