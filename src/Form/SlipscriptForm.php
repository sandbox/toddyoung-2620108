<?php
/**
 * @file
 * Contains \Drupal\slipscript\Form\SlipscriptForm.
 */

namespace Drupal\slipscript\Form;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityForm;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\Query\QueryFactory;
use Drupal\Core\Form\FormStateInterface;

/**
 * SlipscriptForm Class Doc Comment.
 */
class SlipscriptForm extends EntityForm {

  /**
   * The entity query.
   *
   * @param \Drupal\Core\Entity\Query\QueryFactory $entity_query
   *   The entity query.
   */
  public function __construct(QueryFactory $entity_query) {
    $this->entityQuery = $entity_query;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.query')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $slipscript = $this->entity;

    $form['route'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Route'),
      '#maxlength' => 255,
      '#default_value' => $slipscript->route,
      '#description' => $this->t("A single-word \"verb\" for the URL path leading to this script"),
      '#required' => TRUE,
    );

    $form['note'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Note'),
      '#maxlength' => 255,
      '#default_value' => $slipscript->note,
      '#description' => $this->t("Optional note describing the purpose of this route/script."),
      '#required' => FALSE,
    );

    $form['id'] = array(
      '#type' => 'machine_name',
      '#default_value' => $slipscript->id,
      '#machine_name' => array(
        'source' => array('route'),
        'exists' => array($this, 'exist'),
      ),
      '#disabled' => !$slipscript->isNew(),
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $slipscript = $this->entity;
    $status = $slipscript->save();

    if ($status) {
      drupal_set_message($this->t('Saved the %label Slipscript.', array(
        '%label' => $slipscript->label(),
      )));
    }
    else {
      drupal_set_message($this->t('The %label Slipscript was not saved.', array(
        '%label' => $slipscript->label(),
      )));
    }

    $form_state->setRedirect('slipscript.list');
  }

  /**
   * If exist.
   */
  public function exist($id) {
    $entity = $this->entityQuery->get('slipscript')
      ->condition('id', $id)
      ->execute();
    return (bool) $entity;
  }

}
