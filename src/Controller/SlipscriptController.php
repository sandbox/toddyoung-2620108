<?php
/**
 * @file
 * Contains \Drupal\slipscript\Controller\SlipscriptController.
 */

namespace Drupal\slipscript\Controller;

use Drupal\Core\Controller\ControllerBase;

use Drupal\Component\Utility\SafeMarkup;
use Drupal\Component\Utility\String;

class SlipscriptController extends ControllerBase {

  /**
   * {@inheritdoc}
   */
	public function includer($verb, $arg1, $arg2, $arg3, $arg4, $arg5, $arg6, $arg7, $arg8, $arg9) {
	
	  print '<link rel="stylesheet" type="text/css" href="/_slipscripts/_master.css">';
	  print '<link rel="stylesheet" type="text/css" href="/_slipscripts/' . $verb . '/' . $verb . '.css">';
		ob_start();
		include_once '_slipscripts/' . $verb . '/' . $verb . '.inc';
		$output = ob_get_contents();
		ob_end_clean();

    /* This approach no longer working as of D8 RC2, see https://www.drupal.org/node/2391025
		$build = array(
      '#type' => 'markup',
      '#markup' => $output,
			'#allowed_tags' => array('input'), // <!-- current issue is that forms won't make it though...
  	);
		*/
		
		$build = array(
      '#type' => 'inline_template',
      '#template' => $output,
  	);

    return $build;
	}
}
?>


