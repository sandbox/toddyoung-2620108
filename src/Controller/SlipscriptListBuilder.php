<?php
/**
 * @file
 * Contains \Drupal\slipscript\Controller\SlipscriptListBuilder.
 */

namespace Drupal\slipscript\Controller;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * Provides a listing of Slipscripts.
 */
class SlipscriptListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Machine name');
    $header['route'] = $this->t('URL Path (Route)');
    $header['note'] = $this->t('Note');
    return $header + parent::buildHeader();
  }

  public function buildRow(EntityInterface $entity) {
    $row['id'] = $entity->id();
    $row['route'] = $entity->route; // this->getLabel($entity); //entity->label;
    $row['note'] = $entity->note; //print_r($entity, 1);

    return $row + parent::buildRow($entity);
  }

}
?>